﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.V1;
using Entities;
using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Repositories.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        private readonly TodoContext _context;

        public TodoRepository(TodoContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<TodoItem>> GetTodoItemsAsync(PaginationFilter paginationFilter = null)
        {
            if (paginationFilter == null)
            {
                return await _context.TodoItems.ToListAsync();
            }

            var skip = (paginationFilter.PageNumber - 1) * paginationFilter.PageSize;

            return await _context.TodoItems
                .Include(t => t.Subtasks)
                .Skip(skip)
                .Take(paginationFilter.PageSize)
                .ToListAsync();
        }

        public async Task<TodoItem> GetTodoItemAsync(int id)
        {
            return await _context.TodoItems
                .Include(t => t.Subtasks)
                .FirstOrDefaultAsync(t => t.Id == id);
        }

        public void AddTodoItem(TodoItem todoItem)
        {
            if (todoItem == null)
            {
                throw new ArgumentNullException(nameof(todoItem));
            }

            _context.Add(todoItem);
        }

        public async Task<bool> TodoItemExistsAsync(int id)
        {
            return await _context.TodoItems.AnyAsync(t => t.Id == id);
        }

        public async Task<bool> SaveAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void DeleteToDoItem(TodoItem todoItem)
        {
            if (todoItem == null)
            {
                throw new ArgumentNullException(nameof(todoItem));
            }

            _context.Remove(todoItem);
        }
    }
}