﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.V1;
using Entities.Models;

namespace Repositories.Repositories
{
	public interface ITodoRepository
	{
		Task<IEnumerable<TodoItem>> GetTodoItemsAsync(PaginationFilter paginationFilter = null);
		Task<TodoItem> GetTodoItemAsync(int id);
		void AddTodoItem(TodoItem todoItem);
		Task<bool> TodoItemExistsAsync(int id);
		Task<bool> SaveAsync();
		void DeleteToDoItem(TodoItem todoItem);
	}
}