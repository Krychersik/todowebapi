﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Contracts.V1;
using Contracts.V1.Requests;
using Contracts.V1.Responses;
using FluentAssertions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Newtonsoft.Json;
using Xunit;

namespace TodoWebApi.IntegrationTests
{
    public class TodoControllerTests : IntegrationTest
    {
        private async Task<TodoResponse> CreateTodoItemAsync(CreateTodoRequest request)
        {
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Todo.Create, request);
            var createdTodoItemResponse = await response.Content.ReadAsAsync<Response<TodoResponse>>();
            var createdTodoItem = createdTodoItemResponse.Data;

            return createdTodoItem;
        }

        [Fact]
        public async Task Get_Returns_Not_Found_When_Todo_Item_With_Id_Doesnt_Exist_In_The_Database()
        {
            // Arrange
            const int itemId = 3;

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Todo.Get.Replace("{id}", itemId.ToString()));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Get_Returns_Todo_Item_When_Todo_Item_Exists_In_The_Database()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            // Act
            var response =
                await TestClient.GetAsync(ApiRoutes.Todo.Get.Replace("{id}", createdTodoItem.Id.ToString()));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var returnedTodoItem = await response.Content.ReadAsAsync<Response<TodoResponse>>();
            returnedTodoItem.Data.Id.Should().Be(createdTodoItem.Id);
            returnedTodoItem.Data.Title.Should().Be("Title");
            returnedTodoItem.Data.Details.Should().Be("Details");
            returnedTodoItem.Data.IsComplete.Should().Be(false);
            returnedTodoItem.Data.DateTime.Should().Be(dateTime);
        }

        [Fact]
        public async Task GetAll_Returns_A_List_Of_Todo_Items_When_The_Database_Is_Not_Empty()
        {
            // Arrange
            for (var i = 0; i < 2; i++)
            {
                await CreateTodoItemAsync(new CreateTodoRequest
                {
                    Title = $"Title{i + 1}",
                    Details = $"Details{i + 1}",
                    DateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0),
                    IsComplete = false
                });
            }

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Todo.GetAll);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var returnedTodoItems = await response.Content.ReadAsAsync<Response<List<TodoResponse>>>();
            returnedTodoItems.Data.Should().NotBeEmpty();
            returnedTodoItems.Data.Should().HaveCount(2);
            returnedTodoItems.Data[0].Title.Should().Be("Title1");
            returnedTodoItems.Data[1].Title.Should().Be("Title2");
        }

        [Fact]
        public async Task GetAll_Without_Any_Todo_Items_Returns_Empty_Response()
        {
            // Arrange

            // Act
            var response = await TestClient.GetAsync(ApiRoutes.Todo.GetAll);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            (await response.Content.ReadAsAsync<Response<List<TodoResponse>>>()).Data.Should().BeEmpty();
        }

        [Fact]
        public async Task Create_Returns_Bad_Request_When_Request_Body_Is_Null()
        {
            // Arrange

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Todo.Create, (CreateTodoRequest) null);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Create_Returns_Bad_Request_When_Request_Is_NotValid()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var request = new CreateTodoRequest
            {
                Title = null,
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            };

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Todo.Create, request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Create_Returns_Created_When_Request_Is_Valid()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var request = new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            };

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Todo.Create, request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact]
        public async Task Create_Creates_A_Todo_Item()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var request = new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            };

            // Act
            var response = await TestClient.PostAsJsonAsync(ApiRoutes.Todo.Create, request);
            var createdTodoItemResponse = await response.Content.ReadAsAsync<Response<TodoResponse>>();
            var createdTodoItem = createdTodoItemResponse.Data;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            createdTodoItem.Id.Should().Be(createdTodoItem.Id);
            createdTodoItem.Title.Should().Be("Title");
            createdTodoItem.Details.Should().Be("Details");
            createdTodoItem.IsComplete.Should().Be(false);
            createdTodoItem.DateTime.Should().Be(dateTime);
        }

        [Fact]
        public async Task Update_Returns_Not_Found_When_Todo_Item_With_Id_Doesnt_Exist_In_The_Database()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);
            var id = 5;

            var request = new UpdateTodoRequest
            {
                Title = "Updated Title",
                Details = "Updated Details",
                DateTime = dateTime,
                IsComplete = false
            };

            // Act
            var response =
                await TestClient.PutAsJsonAsync(ApiRoutes.Todo.Update.Replace("{id}", id.ToString()), request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Update_Returns_Bad_Request_When_Todo_Item_Could_Not_Be_Updated()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new UpdateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            };

            // Act
            var response =
                await TestClient.PutAsJsonAsync(ApiRoutes.Todo.Update.Replace("{id}", createdTodoItem.Id.ToString()),
                    request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Update_Returns_No_Content_When_Todo_Item_Exists_In_The_Database()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new UpdateTodoRequest
            {
                Title = "Updated Title",
                Details = "Updated Details",
                DateTime = dateTime,
                IsComplete = true
            };

            // Act
            var response =
                await TestClient.PutAsJsonAsync(ApiRoutes.Todo.Update.Replace("{id}", createdTodoItem.Id.ToString()),
                    request);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task Update_Updates_A_Todo_Item()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new UpdateTodoRequest
            {
                Title = "Updated Title",
                Details = "Updated Details",
                DateTime = dateTime,
                IsComplete = true
            };

            // Act
            var response =
                await TestClient.PutAsJsonAsync(ApiRoutes.Todo.Update.Replace("{id}", createdTodoItem.Id.ToString()),
                    request);
            var getResponse =
                await TestClient.GetAsync(ApiRoutes.Todo.Get.Replace("{id}", createdTodoItem.Id.ToString()));
            var updatedTodoItem = await getResponse.Content.ReadAsAsync<Response<TodoResponse>>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
            updatedTodoItem.Data.Id.Should().Be(createdTodoItem.Id);
            updatedTodoItem.Data.Title.Should().Be("Updated Title");
            updatedTodoItem.Data.Details.Should().Be("Updated Details");
            updatedTodoItem.Data.IsComplete.Should().Be(true);
            updatedTodoItem.Data.DateTime.Should().Be(dateTime);
        }

        [Fact]
        public async Task Patch_Returns_Bad_Request_When_Todo_Item_Could_Not_Be_Updated()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = (JsonPatchDocument<UpdateTodoRequest>) null;

            // Act
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            var response =
                await TestClient.PatchAsync(ApiRoutes.Todo.Update.Replace("{id}", createdTodoItem.Id.ToString()),
                    content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact]
        public async Task Patch_Returns_Not_Found_Todo_Item_With_Id_Doesnt_Exist_In_The_Database()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);
            var id = 5;

            await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new JsonPatchDocument<UpdateTodoRequest>
            {
                Operations =
                {
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/title",
                        value = "Replaced Title"
                    }
                }
            };

            // Act
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            var response =
                await TestClient.PatchAsync(ApiRoutes.Todo.Update.Replace("{id}", id.ToString()), content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Patch_Returns_No_Content_When_Todo_Item_Was_Successfully_Updated()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var cratedTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new JsonPatchDocument<UpdateTodoRequest>
            {
                Operations =
                {
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/title",
                        value = "Replaced Title"
                    }
                }
            };

            // Act
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            var response =
                await TestClient.PatchAsync(ApiRoutes.Todo.Update.Replace("{id}", cratedTodoItem.Id.ToString()),
                    content);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task Patch_Updates_A_Todo_Item()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            var request = new JsonPatchDocument<UpdateTodoRequest>
            {
                Operations =
                {
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/title",
                        value = "Replaced Title"
                    }
                }
            };

            // Act
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");

            var response =
                await TestClient.PatchAsync(ApiRoutes.Todo.Update.Replace("{id}", createdTodoItem.Id.ToString()),
                    content);
            var getResponse =
                await TestClient.GetAsync(ApiRoutes.Todo.Get.Replace("{id}", createdTodoItem.Id.ToString()));
            var updatedTodoItem = await getResponse.Content.ReadAsAsync<Response<TodoResponse>>();

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
            updatedTodoItem.Data.Title.Should().Be("Replaced Title");
        }

        [Fact]
        public async Task Delete_Returns_Not_Found_Todo_Item_With_Id_Doesnt_Exist_In_The_Database()
        {
            // Arrange
            var id = 5;

            // Act
            var response = await TestClient.DeleteAsync(ApiRoutes.Todo.Delete.Replace("{id}", id.ToString()));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task Delete_ReturnsNo_Content_When_Todo_Item_Was_Successfully_Deleted()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var cratedTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            // Act
            var response =
                await TestClient.DeleteAsync(ApiRoutes.Todo.Delete.Replace("{id}", cratedTodoItem.Id.ToString()));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task Delete_Deletes_A_Todo_Item()
        {
            // Arrange
            var dateTime = new DateTime(2019, 10, 06, 12, 0, 0, 0);

            var createdTodoItem = await CreateTodoItemAsync(new CreateTodoRequest
            {
                Title = "Title",
                Details = "Details",
                DateTime = dateTime,
                IsComplete = false
            });

            // Act
            var response =
                await TestClient.DeleteAsync(ApiRoutes.Todo.Delete.Replace("{id}", createdTodoItem.Id.ToString()));
            var getResponse =
                await TestClient.GetAsync(ApiRoutes.Todo.Get.Replace("{id}", createdTodoItem.Id.ToString()));

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
            getResponse.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }
}