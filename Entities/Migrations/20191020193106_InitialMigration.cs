﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TodoItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    Details = table.Column<string>(maxLength: 100, nullable: true),
                    DateTime = table.Column<DateTime>(nullable: true),
                    IsComplete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TodoItems", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subtasks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 100, nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    TodoItemId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subtasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subtasks_TodoItems_TodoItemId",
                        column: x => x.TodoItemId,
                        principalTable: "TodoItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "TodoItems",
                columns: new[] { "Id", "DateTime", "Details", "IsComplete", "Title" },
                values: new object[] { 1, null, null, false, "Laundry" });

            migrationBuilder.InsertData(
                table: "TodoItems",
                columns: new[] { "Id", "DateTime", "Details", "IsComplete", "Title" },
                values: new object[] { 2, null, null, false, "Feed the Cat" });

            migrationBuilder.InsertData(
                table: "TodoItems",
                columns: new[] { "Id", "DateTime", "Details", "IsComplete", "Title" },
                values: new object[] { 3, null, null, false, "Cook the dinner" });

            migrationBuilder.InsertData(
                table: "Subtasks",
                columns: new[] { "Id", "IsComplete", "Title", "TodoItemId" },
                values: new object[] { 1, false, "Go to the room", 1 });

            migrationBuilder.CreateIndex(
                name: "IX_Subtasks_TodoItemId",
                table: "Subtasks",
                column: "TodoItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subtasks");

            migrationBuilder.DropTable(
                name: "TodoItems");
        }
    }
}
