﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Entities
{
	public interface ITodoContext
	{
		DbSet<TodoItem> TodoItems { get; set; }
	}
}