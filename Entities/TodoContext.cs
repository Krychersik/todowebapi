﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Entities
{
    public class TodoContext : DbContext, ITodoContext
    {
        public TodoContext(DbContextOptions options) : base(options) { }

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Subtask> Subtasks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<TodoItem>(entity =>
            {
                entity
                    .HasKey(t => t.Id);

                entity
                    .Property(t => t.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity
                    .Property(t => t.Details)
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Subtask>(entity =>
            {
                entity
                    .HasKey(t => t.Id);

                entity
                    .Property(t => t.Title)
                    .IsRequired()
                    .HasMaxLength(100);

                entity
                    .HasOne(t => t.TodoItem)
                    .WithMany(t => t.Subtasks)
                    .HasForeignKey(t => t.TodoItemId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder
                .Entity<TodoItem>()
                .HasData(
                    new TodoItem
                    {
                        Id = 1, Title = "Laundry", Details = null, DateTime = null, IsComplete = false
                    },
                    new TodoItem
                    {
                        Id = 2, Title = "Feed the Cat", Details = null, DateTime = null, IsComplete = false
                    },
                    new TodoItem
                    {
                        Id = 3, Title = "Cook the dinner", Details = null, DateTime = null, IsComplete = false
                    }
                );

            modelBuilder
                .Entity<Subtask>()
                .HasData(new Subtask
                {
                    Id = 1,
                    Title = "Go to the room",
                    IsComplete = false,
                    TodoItemId = 1
                });
        }
    }
}