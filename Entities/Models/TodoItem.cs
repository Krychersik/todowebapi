﻿using System;
using System.Collections.Generic;

namespace Entities.Models
{
	public class TodoItem
	{
		public int Id { get; set; }

		public string Title { get; set; }

		public string Details { get; set; }

		public DateTime? DateTime { get; set; }

		public bool IsComplete { get; set; }

		public List<Subtask> Subtasks { get; set; }
	}
}