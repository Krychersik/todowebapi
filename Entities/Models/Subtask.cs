﻿namespace Entities.Models
{
    public class Subtask
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool IsComplete { get; set; }
        
        public int TodoItemId { get; set; }
        public TodoItem TodoItem { get; set; }
    }
}