﻿using System;
using System.Collections.Generic;

namespace Contracts.V1.Requests
{
    public class CreateTodoRequest
    {
        public string Title { get; set; }

        public string Details { get; set; }

        public DateTime? DateTime { get; set; }

        public bool IsComplete { get; set; }

        public List<SubtaskRequest> Subtasks { get; set; }
    }
}