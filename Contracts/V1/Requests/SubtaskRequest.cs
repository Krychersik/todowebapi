﻿namespace Contracts.V1.Requests
{
    public class SubtaskRequest
    {
        public string Title { get; set; }

        public bool IsComplete { get; set; }
    }
}