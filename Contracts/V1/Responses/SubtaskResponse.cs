﻿namespace Contracts.V1.Responses
{
    public class SubtaskResponse
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool IsComplete { get; set; }
    }
}