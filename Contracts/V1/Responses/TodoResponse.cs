﻿using System;
using System.Collections.Generic;

namespace Contracts.V1.Responses
{
    public class TodoResponse
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Details { get; set; }

        public DateTime? DateTime { get; set; }

        public bool IsComplete { get; set; }

        public List<SubtaskResponse> Subtasks { get; set; }
    }
}