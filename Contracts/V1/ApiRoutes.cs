﻿namespace Contracts.V1
{
    public static class ApiRoutes
    {
        public const string Root = "api";
        public const string Version = "v1";
        public const string Base = Root + "/" + Version;

        public static class Todo
        {
            public const string GetAll = Base + "/todo";
            public const string Update = Base + "/todo/{id}";
            public const string Delete = Base + "/todo/{id}";
            public const string Get = Base + "/todo/{id}";
            public const string Create = Base + "/todo";
        }
    }
}