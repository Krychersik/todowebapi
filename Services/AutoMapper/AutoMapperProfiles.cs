﻿using AutoMapper;
using Contracts.V1.Requests;
using Contracts.V1.Responses;
using Entities.Models;

namespace Services.AutoMapper
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<TodoResponse, TodoItem>();

            CreateMap<TodoItem, TodoResponse>()
                .ForMember(dest => dest.Details, options => options.NullSubstitute(string.Empty));

            CreateMap<CreateTodoRequest, TodoItem>();

            CreateMap<UpdateTodoRequest, TodoItem>();
            CreateMap<TodoItem, UpdateTodoRequest>();

            CreateMap<Subtask, SubtaskResponse>().ReverseMap();
            CreateMap<SubtaskRequest, Subtask>().ReverseMap();
        }
    }
}