﻿using AutoMapper;
using Contracts.V1;
using Contracts.V1.Requests.Queries;

namespace Services.AutoMapper
{
    public class RequestToDomainProfile : Profile
    {
        public RequestToDomainProfile()
        {
            CreateMap<PaginationQuery, PaginationFilter>();
        }
    }
}