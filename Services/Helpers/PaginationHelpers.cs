﻿using System.Collections.Generic;
using System.Linq;
using Contracts.V1;
using Contracts.V1.Requests.Queries;
using Contracts.V1.Responses;
using Services.Services;

namespace Services.Helpers
{
    public class PaginationHelpers
    {
        public static PagedResponse<T> CreatePaginatedResponse<T>(IUriService uriService,
            PaginationFilter paginationFilter,
            IEnumerable<T> response)
        {
            var nextPage = paginationFilter.PageNumber >= 1
                ? uriService.GetAllTodoItemsUri(new PaginationQuery(paginationFilter.PageNumber + 1,
                    paginationFilter.PageSize)).ToString()
                : null;

            var previousPage = paginationFilter.PageNumber - 1 >= 1
                ? uriService.GetAllTodoItemsUri(new PaginationQuery(paginationFilter.PageNumber - 1,
                    paginationFilter.PageSize)).ToString()
                : null;

            return new PagedResponse<T>
            {
                Data = response,
                PageNumber = paginationFilter.PageNumber >= 1 ? paginationFilter.PageNumber : (int?) null,
                PageSize = paginationFilter.PageSize >= 1 ? paginationFilter.PageSize : (int?) null,
                PreviousPage = previousPage,
                NextPage = response.Any() ? nextPage : null
            };
        }
    }
}