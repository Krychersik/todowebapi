﻿using System;
using Contracts.V1;
using Contracts.V1.Requests.Queries;
using Microsoft.AspNetCore.WebUtilities;

namespace Services.Services
{
    public class UriService : IUriService
    {
        private readonly string _baseUri; 

        public UriService(string baseUri)
        {
            _baseUri = baseUri;
        }

        public Uri GetTodoItemUri(string id)
        {
            return new Uri(_baseUri + ApiRoutes.Todo.Get.Replace("{id}", id));
        }

        public Uri GetAllTodoItemsUri(PaginationQuery paginationQuery = null)
        {
            var uri = new Uri(_baseUri);

            if (paginationQuery == null)
            {
                return uri;
            }

            var modifiedUri = QueryHelpers.AddQueryString(_baseUri, "pageNumber", paginationQuery.PageNumber.ToString());
            modifiedUri = QueryHelpers.AddQueryString(modifiedUri, "pageSize", paginationQuery.PageSize.ToString());

            return new Uri(modifiedUri);
        }
    }
}