﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.V1;
using Contracts.V1.Requests;
using Contracts.V1.Responses;
using Entities.Models;
using Microsoft.AspNetCore.JsonPatch;
using Repositories.Repositories;

namespace Services.Services
{
    public class TodoService : ITodoService
    {
        private readonly IMapper _mapper;
        private readonly ITodoRepository _repository;

        public TodoService(ITodoRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TodoResponse>> GetTodoItems(PaginationFilter paginationFilter = null)
        {
            var todoItemEntities = await _repository.GetTodoItemsAsync(paginationFilter);
            var todoItemViewModels = _mapper.Map<IEnumerable<TodoResponse>>(todoItemEntities);

            return todoItemViewModels;
        }

        public async Task<TodoResponse> GetTodoItem(int id)
        {
            var todoItemEntity = await _repository.GetTodoItemAsync(id);
            var todoItemViewModel = _mapper.Map<TodoResponse>(todoItemEntity);

            return todoItemViewModel;
        }

        public async Task<TodoResponse> CreateTodoItem(CreateTodoRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var todoItemEntity = _mapper.Map<TodoItem>(request);
            _repository.AddTodoItem(todoItemEntity);

            if (!await SaveChangesAsync())
            {
                return null;
            }

            var todoItemViewModel = _mapper.Map<TodoResponse>(todoItemEntity);

            return todoItemViewModel;
        }

        public async Task<bool> UpdateTodoItem(int id, UpdateTodoRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var todoItemEntity = await _repository.GetTodoItemAsync(id);

            if (todoItemEntity == null)
            {
                return false;
            }

            _mapper.Map(request, todoItemEntity);

            return await SaveChangesAsync();
        }

        public async Task<bool> TodoItemExists(int id)
        {
            return await _repository.TodoItemExistsAsync(id);
        }

        public async Task<bool> PartiallyUpdateTodoItem(int id, JsonPatchDocument<UpdateTodoRequest> patchDocument)
        {
            if (patchDocument == null)
            {
                return false;
            }

            var todoItemEntity = await _repository.GetTodoItemAsync(id);

            if (todoItemEntity == null)
            {
                return false;
            }

            var todoItemToPatch = _mapper.Map<UpdateTodoRequest>(todoItemEntity);

            try
            {
                patchDocument.ApplyTo(todoItemToPatch);
                _mapper.Map(todoItemToPatch, todoItemEntity);

                return await SaveChangesAsync();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteToDoItem(int id)
        {
            var todoItemEntity = await _repository.GetTodoItemAsync(id);

            if (todoItemEntity == null)
            {
                return false;
            }

            _repository.DeleteToDoItem(todoItemEntity);

            return await SaveChangesAsync();
        }

        private async Task<bool> SaveChangesAsync()
        {
            return await _repository.SaveAsync();
        }
    }
}