﻿using System;
using Contracts.V1.Requests.Queries;

namespace Services.Services
{
    public interface IUriService
    {
        Uri GetTodoItemUri(string id);

        Uri GetAllTodoItemsUri(PaginationQuery paginationQuery = null);
    }
}