﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.V1;
using Contracts.V1.Requests;
using Contracts.V1.Responses;
using Microsoft.AspNetCore.JsonPatch;

namespace Services.Services
{
	public interface ITodoService
	{
		Task<IEnumerable<TodoResponse>> GetTodoItems(PaginationFilter paginationFilter = null);
		Task<TodoResponse> GetTodoItem(int id);
		Task<TodoResponse> CreateTodoItem(CreateTodoRequest request);
		Task<bool> UpdateTodoItem(int id, UpdateTodoRequest request);
		Task<bool> TodoItemExists(int id);
		Task<bool> PartiallyUpdateTodoItem(int id, JsonPatchDocument<UpdateTodoRequest> patchDocument);
		Task<bool> DeleteToDoItem(int id);
	}
}