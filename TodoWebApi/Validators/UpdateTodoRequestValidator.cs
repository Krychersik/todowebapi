﻿using Contracts.V1.Requests;
using FluentValidation;

namespace TodoWebApi.Validators
{
    public class UpdateTodoRequestValidator : AbstractValidator<UpdateTodoRequest>
    {
        public UpdateTodoRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50)
                .Matches("^[a-zA-Z0-9 ]*$");
        }
    }
}