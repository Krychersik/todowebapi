﻿using Contracts.V1.Requests;
using FluentValidation;

namespace TodoWebApi.Validators
{
    public class CreateTodoRequestValidator : AbstractValidator<CreateTodoRequest>
    {
        public CreateTodoRequestValidator()
        {
            RuleFor(x => x.Title)
                .NotNull()
                .NotEmpty()
                .MaximumLength(50)
                .Matches("^[a-zA-Z0-9 ]*$");
        }
    }
}