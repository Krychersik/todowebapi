﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.V1;
using Contracts.V1.Requests;
using Contracts.V1.Requests.Queries;
using Contracts.V1.Responses;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Microsoft.AspNetCore.Mvc;
using Services.Helpers;
using Services.Services;
using Swashbuckle.AspNetCore.Filters;
using TodoWebApi.SwaggerExamples.Requests;

namespace TodoWebApi.Controllers.V1
{
    [Produces("application/json")]
    public class TodoController : Controller
    {
        private readonly ITodoService _todoService;
        private readonly IMapper _mapper;
        private readonly IUriService _uriService;

        public TodoController(ITodoService todoService, IMapper mapper, IUriService uriService)
        {
            _todoService = todoService;
            _mapper = mapper;
            _uriService = uriService;
        }

        /// <summary>
        ///     Returns a Todo Item with {id}
        /// </summary>
        /// <param name="id">id of a Todo item</param>
        /// <response code="200">Returns a Todo Item with given id</response>
        [ProducesResponseType(typeof(Response<TodoResponse>), 200)]
        [HttpGet(ApiRoutes.Todo.Get)]
        public async Task<ActionResult<Response<TodoResponse>>> Get([FromRoute] int id)
        {
            if (!await _todoService.TodoItemExists(id))
            {
                return NotFound();
            }

            var todoItemViewModel = await _todoService.GetTodoItem(id);
            var response = new Response<TodoResponse>(todoItemViewModel);

            return response;
        }

        /// <summary>
        ///     Returns all Todo Items
        /// </summary>
        /// <response code="200">Returns all Todo Items</response>
        [ProducesResponseType(typeof(PagedResponse<TodoResponse>), 200)]
        [HttpGet(ApiRoutes.Todo.GetAll)]
        public async Task<ActionResult<PagedResponse<TodoResponse>>> GetAll([FromQuery] PaginationQuery paginationQuery)
        {
            var paginationFilter = _mapper.Map<PaginationFilter>(paginationQuery);
            var todoItemViewModels = await _todoService.GetTodoItems(paginationFilter);

            if (paginationFilter == null || paginationFilter.PageNumber < 1 || paginationFilter.PageSize < 1)
            {
                return new PagedResponse<TodoResponse>(todoItemViewModels);
            }

            var paginationResponse =
                PaginationHelpers.CreatePaginatedResponse(_uriService, paginationFilter, todoItemViewModels);

            return paginationResponse;
        }

        /// <summary>
        ///     Creates a Todo Item
        /// </summary>
        /// <response code="201">Creates a Todo Item</response>
        /// <response code="400">Unable to create the todo item</response>
        [ProducesResponseType(typeof(Response<TodoResponse>), 201)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [Consumes("application/json")]
        [HttpPost(ApiRoutes.Todo.Create)]
        public async Task<IActionResult> Create([FromBody] CreateTodoRequest request)
        {
            var todoItemViewModel = await _todoService.CreateTodoItem(request);

            if (todoItemViewModel == null)
            {
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                        {new ErrorModel {Message = "An error occurred during Todo Item creation."}}
                });
            }

            var response = new Response<TodoResponse>(todoItemViewModel);
            var locationUri = _uriService.GetTodoItemUri(todoItemViewModel.Id.ToString());

            return Created(locationUri, response);
        }

        /// <summary>
        ///     Updates a Todo Item with {id}
        /// </summary>
        /// <response code="204">Updates a Todo Item</response>
        /// <response code="400">Unable to update the todo item</response>
        /// <response code="404">Couldn't find a Todo Item with {id}</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        [Consumes("application/json")]
        [HttpPut(ApiRoutes.Todo.Update)]
        public async Task<IActionResult> Update(int id, [FromBody] UpdateTodoRequest request)
        {
            if (!await _todoService.TodoItemExists(id))
            {
                return NotFound();
            }

            var updated = await _todoService.UpdateTodoItem(id, request);

            if (!updated)
            {
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                        {new ErrorModel {Message = "An error occurred during Todo Item updating."}}
                });
            }

            return NoContent();
        }

        /// <summary>
        ///     Partially updates a Todo Item with {id}
        /// </summary>
        /// <response code="204">Partially updates a Todo Item</response>
        /// <response code="400">Unable to partially update the todo item</response>
        /// <response code="404">Couldn't find a Todo Item with {id}</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        [SwaggerRequestExample(typeof(Operation), typeof(PartiallyUpdateTodoRequestExample))]
        [Consumes("application/json")]
        [HttpPatch(ApiRoutes.Todo.Update)]
        public async Task<IActionResult> PartiallyUpdate(int id,
            [FromBody] JsonPatchDocument<UpdateTodoRequest> patchDocument)
        {
            if (!await _todoService.TodoItemExists(id))
            {
                return NotFound();
            }

            var updated = await _todoService.PartiallyUpdateTodoItem(id, patchDocument);

            if (!updated)
            {
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                        {new ErrorModel {Message = "An error occurred during Todo Item updating."}}
                });
            }

            return NoContent();
        }

        /// <summary>
        ///     Deletes a Todo Item with {id}
        /// </summary>
        /// <response code="204">Deletes a Todo Item</response>
        /// <response code="400">Unable to delete the todo item</response>
        /// <response code="404">Couldn't find a Todo Item with {id}</response>
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResponse), 400)]
        [ProducesResponseType(404)]
        [HttpDelete(ApiRoutes.Todo.Delete)]
        public async Task<IActionResult> Delete(int id)
        {
            if (!await _todoService.TodoItemExists(id))
            {
                return NotFound();
            }

            var deleted = await _todoService.DeleteToDoItem(id);

            if (!deleted)
            {
                return BadRequest(new ErrorResponse
                {
                    Errors = new List<ErrorModel>
                        {new ErrorModel {Message = "An error occurred during Todo Item deleting."}}
                });
            }

            return NoContent();
        }
    }
}