﻿using System.Collections.Generic;
using Contracts.V1.Responses;
using Swashbuckle.AspNetCore.Filters;

namespace TodoWebApi.SwaggerExamples.Responses
{
    public class ErrorResponseExample : IExamplesProvider<ErrorResponse>
    {
        public ErrorResponse GetExamples()
        {
            return new ErrorResponse
            {
                Errors = new List<ErrorModel>
                {
                    new ErrorModel
                    {
                        Message = "Error message",
                        FieldName = "Error field name"
                    }
                }
            };
        }
    }
}