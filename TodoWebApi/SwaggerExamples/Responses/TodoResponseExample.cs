﻿using System;
using System.Collections.Generic;
using Contracts.V1.Responses;
using Swashbuckle.AspNetCore.Filters;

namespace TodoWebApi.SwaggerExamples.Responses
{
    public class TodoResponseExample : IExamplesProvider<TodoResponse>
    {
        public TodoResponse GetExamples()
        {
            return new TodoResponse
            {
                Id = 1,
                Title = "Some title",
                Details = "Some details",
                DateTime = new DateTime(2019, 10, 12, 12, 0, 0, 0),
                IsComplete = false,
                Subtasks = new List<SubtaskResponse>
                {
                    new SubtaskResponse
                    {
                        Id = 1,
                        Title = "Subtask Title",
                        IsComplete = false
                    }
                }
            };
        }
    }
}