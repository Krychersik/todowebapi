﻿using System;
using System.Collections.Generic;
using Contracts.V1.Requests;
using Swashbuckle.AspNetCore.Filters;

namespace TodoWebApi.SwaggerExamples.Requests
{
    public class UpdateTodoRequestExample : IExamplesProvider<UpdateTodoRequest>
    {
        public UpdateTodoRequest GetExamples()
        {
            return new UpdateTodoRequest
            {
                Title = "Some title",
                Details = "Some details",
                DateTime = new DateTime(2019, 10, 12, 12, 0, 0, 0),
                IsComplete = false,
                Subtasks = new List<SubtaskRequest>
                {
                    new SubtaskRequest
                    {
                        Title = "Subtask Title",
                        IsComplete = false
                    }
                }
            };
        }
    }
}