﻿using System;
using System.Collections.Generic;
using Contracts.V1.Requests;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.JsonPatch.Operations;
using Swashbuckle.AspNetCore.Filters;

namespace TodoWebApi.SwaggerExamples.Requests
{
    public class PartiallyUpdateTodoRequestExample : IExamplesProvider<JsonPatchDocument<UpdateTodoRequest>>
    {
        public JsonPatchDocument<UpdateTodoRequest> GetExamples()
        {
            return new JsonPatchDocument<UpdateTodoRequest>
            {
                Operations =
                {
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/title",
                        value = "Replaced Title"
                    },
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/details",
                        value = "Replaced Details"
                    },
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/isComplete",
                        value = false
                    },
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/dateTime",
                        value = new DateTime(2019, 10, 12, 12, 0, 0, 0)
                    },
                    new Operation<UpdateTodoRequest>
                    {
                        op = "replace",
                        path = "/subtasks",
                        value = new List<SubtaskRequest>
                        {
                            new SubtaskRequest
                            {
                                Title = "Subtask Title",
                                IsComplete = false
                            }
                        }
                    }
                }
            };
        }
    }
}